from django.conf.urls import url
#from rest_framework.urlpatterns import format_suffix_patterns
from profile_related.views import ( AccountListAPIView, AccountLogin, AccountDetailsAPIView,
AddressListAPIView, AddressDetailsAPIView, AccountCheckEmail )

urlpatterns = [
    url(r'^account/$', AccountListAPIView.as_view(), name='list'),
    url(r'^account/login/$', AccountLogin.as_view(), name='login'),
    url(r'^account/checkmail/$', AccountCheckEmail.as_view(), name='checkemail'),
    url(r'^account/(?P<pk>[a-z0-9-]+)/$', AccountDetailsAPIView.as_view(), name='details'),
    url(r'^address/$', AddressListAPIView.as_view(), name='addresslist'),
    url(r'^address/(?P<pk>[a-z0-9-]+)/$', AddressDetailsAPIView.as_view(), name='addressdetails'),
]

#urlpatterns = format_suffix_patterns(urlpatterns)
