from django.db import models
from djangodemo.basemodel import CommonInfo
from djangodemo.siteconfig import TIMEZONE_OPTS
from crum import get_current_user

class Account(CommonInfo):
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=50, blank=True, null=True,default=None)
    #password = models.CharField(max_length=50, blank=True, null=True,default=None)
    timezone = models.CharField(max_length=256, blank=True, null=True,default=None, choices=TIMEZONE_OPTS)
    phone = models.BigIntegerField(blank=True, null=True,default=None)
    def __str__(self):
        return self.name

class Address(CommonInfo):
    address_1 = models.CharField(max_length=500, blank=True, null=True,default=None)
    address_2 = models.CharField(max_length=500, blank=True, null=True,default=None)
    city = models.CharField(max_length=50, blank=True, null=True,default=None)
    state_code = models.CharField(max_length=255, blank=True, null=True,default=None)
    postal_code = models.CharField(max_length=10, blank=True, null=True,default=None)
    billing_account = models.ForeignKey(Account, related_name='ba_address',on_delete=models.CASCADE, blank=True, null=True,default=None)
    def __str__(self):
        return self.address_1
    class Meta:
        verbose_name_plural = 'Addresses'
