from django.contrib import admin
from profile_related.models import Account, Address
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter

class AccountAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'phone', 'timezone', 'created_at']
    search_fields = ['name', 'email', 'phone']
    list_filter = [('created_at', DateRangeFilter),]
    date_hierarchy = 'created_at'
    ordering = ['-created_at']
    list_per_page = 50
    pass
admin.site.register(Account, AccountAdmin)

class AddressAdmin(admin.ModelAdmin):
    list_display = ['address_1', 'city', 'postal_code', 'billing_account','created_at']
    date_hierarchy = 'created_at'
    ordering = ['-created_at']
    list_per_page = 50
    pass
admin.site.register(Address, AddressAdmin)
