from rest_framework.serializers import ModelSerializer
from profile_related.models import Account, Address

class AccountSerializer(ModelSerializer):
    class Meta:
        model = Account
        fields = ('id', 'name', 'email', 'timezone', 'phone')

class AccountLoginSerializer(ModelSerializer):
    class Meta:
        model = Account
        fields = ('id', 'name', 'email', 'created_at')

class AddressSerializer(ModelSerializer):
    class Meta:
        model = Address
        fields = ('id', 'address_1', 'address_2', 'city', 'state_code', 'postal_code', 'created_at')
