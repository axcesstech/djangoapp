from django.db.models import Q
import django_filters as df
from djangodemo.functions import customFilterFunction
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.filters import SearchFilter, OrderingFilter
from profile_related.models import Account, Address
from profile_related.serializers import AccountSerializer, AccountLoginSerializer, AddressSerializer
from rest_framework.views import APIView
from django.http import Http404
from rest_framework import status
from rest_framework.response import Response

class AccountListAPIView(ListCreateAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

class AccountDetailsAPIView(RetrieveUpdateDestroyAPIView):
    #queryset = Account.objects.all()
    serializer_class = AccountSerializer

    def get_object(self, id):
        try:
            return Account.objects.filter(id=id).first()
        except Account.DoesNotExist:
            raise Http404

    def get(self, *args, **kwargs):
        account = self.get_object(self.kwargs['pk'])
        serializer = AccountSerializer(account)
        return Response(serializer.data)

    def patch(self, *args, **kwargs):
        account = self.get_object(self.kwargs['pk'])
        serializer = AccountSerializer(account, data= self.request.data , partial=True)
        if serializer.is_valid():
            serializer.save()
        return Response({'data':serializer.data, 'status':True, 'statusCode':status.HTTP_200_OK, 'message':'Success'}, status=status.HTTP_200_OK)

class AccountLogin(APIView):
    def get_object(self, email):
        try:
            return Account.objects.filter(email=email).first()
        except Account.DoesNotExist:
            raise Http404

    def post(self, request, format=None):
        account = self.get_object(request.data['email'])
        if not account:
            return Response({'message':'Email doesn\'t exist'}, status=status.HTTP_404_NOT_FOUND)
        serializer = AccountLoginSerializer(account)
        return Response(serializer.data, status=status.HTTP_200_OK)

class AccountCheckEmail(APIView):
    def get_object(self, email):
        try:
            return Account.objects.filter(email=email).first()
        except Account.DoesNotExist:
            raise Http404

    def post(self, request, format=None):
        if not request.data['email']:
            return Response({'message':'Email field is required'}, status=status.HTTP_400_BAD_REQUEST)
        account = self.get_object(request.data['email'])
        if account:
            return Response({'message':'Email already exists'}, status=status.HTTP_409_CONFLICT)
        return Response({'message':'Email doesn\'t exist'}, status=status.HTTP_200_OK )

class AddressFilter(df.FilterSet):
    class Meta:
        model = Address
        fields = ['billing_account']

class AddressListAPIView(ListCreateAPIView):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer
    filter_class = AddressFilter
    search_fields = ['address_1', 'address_2', 'city']
    ordering_fields = ('created_at')
    ordering = ('-created_at',)

class AddressDetailsAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer
