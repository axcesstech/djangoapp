# Generated by Django 2.0 on 2019-03-13 10:46

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='date created')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='date updated')),
                ('name', models.CharField(max_length=50)),
                ('email', models.CharField(blank=True, default=None, max_length=50, null=True)),
                ('password', models.CharField(blank=True, default=None, max_length=50, null=True)),
                ('timezone', models.CharField(blank=True, choices=[('Pacific/Midway', '(GMT-11:00) Midway Island, Samoa'), ('America/Adak', '(GMT-10:00) Hawaii-Aleutian'), ('Pacific/Marquesas', '(GMT-09:30) Marquesas Islands'), ('Pacific/Gambier', '(GMT-09:00) Gambier Islands'), ('America/Anchorage', '(GMT-09:00) Alaska'), ('America/Ensenada', '(GMT-08:00) Tijuana, Baja California'), ('America/Los_Angeles', '(GMT-08:00) Pacific Time (US & Canada)'), ('America/Denver', '(GMT-07:00) Mountain Time (US & Canada)'), ('America/Chihuahua', '(GMT-07:00) Chihuahua, La Paz, Mazatlan'), ('America/Dawson_Creek', '(GMT-07:00) Arizona'), ('America/Belize', '(GMT-06:00) Saskatchewan, Central America'), ('America/Cancun', '(GMT-06:00) Guadalajara, Mexico City, Monterrey'), ('Chile/EasterIsland', '(GMT-06:00) Easter Island'), ('America/Chicago', '(GMT-06:00) Central Time (US & Canada)'), ('America/New_York', '(GMT-05:00) Eastern Time (US & Canada)'), ('America/Havana', '(GMT-05:00) Cuba'), ('America/Bogota', '(GMT-05:00) Bogota, Lima, Quito, Rio Branco'), ('America/Caracas', '(GMT-04:30) Caracas'), ('America/Santiago', '(GMT-04:00) Santiago'), ('America/La_Paz', '(GMT-04:00) La Paz'), ('Atlantic/Stanley', '(GMT-04:00) Faukland Islands'), ('America/Campo_Grande', '(GMT-04:00) Brazil'), ('America/Goose_Bay', '(GMT-04:00) Atlantic Time (Goose Bay)'), ('America/Glace_Bay', '(GMT-04:00) Atlantic Time (Canada)'), ('America/St_Johns', '(GMT-03:30) Newfoundland'), ('America/Araguaina', '(GMT-03:00) UTC-3'), ('America/Montevideo', '(GMT-03:00) Montevideo'), ('America/Miquelon', '(GMT-03:00) Miquelon, St. Pierre'), ('America/Godthab', '(GMT-03:00) Greenland'), ('America/Argentina/Buenos_Aires', '(GMT-03:00) Buenos Aires'), ('America/Sao_Paulo', '(GMT-03:00) Brasilia'), ('America/Noronha', '(GMT-02:00) Mid-Atlantic'), ('Atlantic/Cape_Verde', '(GMT-01:00) Cape Verde Is.'), ('Atlantic/Azores', '(GMT-01:00) Azores'), ('Europe/Belfast', '(GMT) Greenwich Mean Time : Belfast'), ('Europe/Dublin', '(GMT) Greenwich Mean Time : Dublin'), ('Europe/Lisbon', '(GMT) Greenwich Mean Time : Lisbon'), ('Europe/London', '(GMT) Greenwich Mean Time : London'), ('Africa/Abidjan', '(GMT) Monrovia, Reykjavik'), ('Europe/Amsterdam', '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna'), ('Europe/Belgrade', '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague'), ('Europe/Brussels', '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris'), ('Africa/Algiers', '(GMT+01:00) West Central Africa'), ('Africa/Windhoek', '(GMT+01:00) Windhoek'), ('Asia/Beirut', '(GMT+02:00) Beirut'), ('Africa/Cairo', '(GMT+02:00) Cairo'), ('Asia/Gaza', '(GMT+02:00) Gaza'), ('Africa/Blantyre', '(GMT+02:00) Harare, Pretoria'), ('Asia/Jerusalem', '(GMT+02:00) Jerusalem'), ('Europe/Minsk', '(GMT+02:00) Minsk'), ('Asia/Damascus', '(GMT+02:00) Syria'), ('Europe/Moscow', '(GMT+03:00) Moscow, St. Petersburg, Volgograd'), ('Africa/Addis_Ababa', '(GMT+03:00) Nairobi'), ('Asia/Tehran', '(GMT+03:30) Tehran'), ('Asia/Dubai', '(GMT+04:00) Abu Dhabi, Muscat'), ('Asia/Yerevan', '(GMT+04:00) Yerevan'), ('Asia/Kabul', '(GMT+04:30) Kabul'), ('Asia/Yekaterinburg', '(GMT+05:00) Ekaterinburg'), ('Asia/Tashkent', '(GMT+05:00) Tashkent'), ('Asia/Kolkata', '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi'), ('Asia/Katmandu', '(GMT+05:45) Kathmandu'), ('Asia/Dhaka', '(GMT+06:00) Astana, Dhaka'), ('Asia/Novosibirsk', '(GMT+06:00) Novosibirsk'), ('Asia/Rangoon', '(GMT+06:30) Yangon (Rangoon)'), ('Asia/Bangkok', '(GMT+07:00) Bangkok, Hanoi, Jakarta'), ('Asia/Krasnoyarsk', '(GMT+07:00) Krasnoyarsk'), ('Asia/Hong_Kong', '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi'), ('Asia/Irkutsk', '(GMT+08:00) Irkutsk, Ulaan Bataar'), ('Australia/Perth', '(GMT+08:00) Perth'), ('Australia/Eucla', '(GMT+08:45) Eucla'), ('Asia/Tokyo', '(GMT+09:00) Osaka, Sapporo, Tokyo'), ('Asia/Seoul', '(GMT+09:00) Seoul'), ('Asia/Yakutsk', '(GMT+09:00) Yakutsk'), ('Australia/Adelaide', '(GMT+09:30) Adelaide'), ('Australia/Darwin', '(GMT+09:30) Darwin'), ('Australia/Brisbane', '(GMT+10:00) Brisbane'), ('Australia/Hobart', '(GMT+10:00) Hobart'), ('Asia/Vladivostok', '(GMT+10:00) Vladivostok'), ('Australia/Lord_Howe', '(GMT+10:30) Lord Howe Island'), ('Asia/Magadan', '(GMT+11:00) Magadan'), ('Pacific/Norfolk', '(GMT+11:30) Norfolk Island'), ('Asia/Anadyr', '(GMT+12:00) Anadyr, Kamchatka'), ('Pacific/Auckland', '(GMT+12:00) Auckland, Wellington'), ('Pacific/Chatham', '(GMT+12:45) Chatham Islands'), ('Pacific/Tongatapu', "(GMT+13:00) Nuku'alofa"), ('Pacific/Kiritimati', '(GMT+14:00) Kiritimati')], default=None, max_length=256, null=True)),
                ('phone', models.BigIntegerField(blank=True, default=None, null=True)),
                ('created_by', models.ForeignKey(default=None, editable=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='account_created_by', to=settings.AUTH_USER_MODEL)),
                ('updated_by', models.ForeignKey(default=None, editable=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='account_updated_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='date created')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='date updated')),
                ('address_1', models.CharField(blank=True, default=None, max_length=500, null=True)),
                ('address_2', models.CharField(blank=True, default=None, max_length=500, null=True)),
                ('city', models.CharField(blank=True, default=None, max_length=50, null=True)),
                ('state_code', models.CharField(blank=True, default=None, max_length=255, null=True)),
                ('postal_code', models.CharField(blank=True, default=None, max_length=10, null=True)),
                ('billing_account', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ba_address', to='profile_related.Account')),
                ('created_by', models.ForeignKey(default=None, editable=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='address_created_by', to=settings.AUTH_USER_MODEL)),
                ('updated_by', models.ForeignKey(default=None, editable=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='address_updated_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Addresses',
            },
        ),
    ]
