from django.apps import AppConfig


class ProfileRelatedConfig(AppConfig):
    name = 'profile_related'
    verbose_name = 'Profiles'
