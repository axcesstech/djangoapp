import uuid
from django.db import models

class CommonInfo(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField('date created', auto_now_add=True, editable=False)
    updated_at = models.DateTimeField('date updated', auto_now=True, editable=False)
    created_by = models.ForeignKey('auth.User',on_delete=models.DO_NOTHING, editable=False, null=True, related_name='%(class)s_created_by', default=None)
    updated_by = models.ForeignKey('auth.User',on_delete=models.DO_NOTHING, editable=False, null=True, related_name='%(class)s_updated_by', default=None)
    # deleted
    class Meta:
        abstract = True
