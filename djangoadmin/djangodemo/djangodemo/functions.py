from rest_framework.response import Response
from rest_framework import status
import requests, json
import django_filters as df

def okResp(data):
    return Response({'data':data, 'status':True, 'statusCode':status.HTTP_200_OK, 'message':'Success'}, status=status.HTTP_200_OK)

def createdResp(data):
    return Response({'data':data, 'status':True, 'statusCode':status.HTTP_201_CREATED, 'message':'Success'}, status=status.HTTP_201_CREATED)

def badReqResp(message):
    return Response({'data':'', 'status':False, 'statusCode':status.HTTP_400_BAD_REQUEST, 'message':message}, status=status.HTTP_400_BAD_REQUEST)

def notFoundResp(message):
    return Response({'data':'', 'status':False, 'statusCode':status.HTTP_404_NOT_FOUND, 'message':message}, status=status.HTTP_404_NOT_FOUND)

def conflictResp(message):
    return Response({'data':'', 'status':False, 'statusCode':status.HTTP_409_CONFLICT, 'message':message}, status=status.HTTP_409_CONFLICT)

def unavailableResp(data):
    return Response({'data':data, 'status':False, 'statusCode':status.HTTP_503_SERVICE_UNAVAILABLE, 'message':'Failed'}, status=status.HTTP_503_SERVICE_UNAVAILABLE)

class customFilterFunction (df.BaseInFilter, df.CharFilter):
    pass
