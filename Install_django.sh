#!/bin/bash
yum update -y
# add user
sudo useradd -m -d /home/djangouser djangouser
sudo echo 'djangouser	ALL=(ALL) 	ALL' /etc/sudoers
# copy files from temp to django directory
cp -R /tmp/djangoapp/* /home/djangouser/
# install wget
yum install wget -y
# download and install epel packages
wget http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -ivh epel-release-latest-7.noarch.rpm
yum install -y epel-release
#install python 3.6
yum install -y python36
# install python setup tools, virtualenv and other related packages
yum install -y python36-setuptools
easy_install-3.6 pip
python36 -m pip install virtualenv
alias python=python36
yum install python36-virtualenv.noarch -y
yum install python36-test.x86_64 -y
yum install python36-pip.noarch -y
yum install python36-setuptools.noarch -y
yum install python36-libs.x86_64 -y

# download and install mysql 
wget https://dev.mysql.com/get/mysql80-community-release-el6-1.noarch.rpm
yum localinstall mysql80-community-release-el6-1.noarch.rpm -y
yum-config-manager --disable mysql80-community -y
yum-config-manager --enable mysql56-community -y
yum install mysql-community-server -y
# start mysql with root user
service mysqld start && mysqladmin --user=root password "root"
# create database
mysql -u root -proot  -e "CREATE DATABASE djangoadmin"
# create mysql user and grant access for above database
mysql -u root -proot -e "CREATE USER 'djangouser'@'%' IDENTIFIED BY 'djangouser'; CREATE USER  'djangouser'@'localhost' IDENTIFIED BY 'djangouser';GRANT ALL PRIVILEGES ON *.* TO 'djangouser'@'%';GRANT ALL PRIVILEGES ON *.* TO 'djangouser'@'localhost';FLUSH PRIVILEGES;"
cd /home/djangouser/
# create required folders with djangouser as owner
su -c "mkdir venvs" djangouser
sudo chown djangouser:djangouser venvs
# run virtualenv command as djangouser
runuser -l djangouser -c 'virtualenv /home/djangouser/venvs/django-py36'
cd /home/djangouser
# active virtual environment for django
source /home/djangouser/venvs/django-py36/bin/activate
# install gunicorn and django inside virtual environment
su -c "pip install gunicorn"  djangouser
su -c "easy_install django" djangouser
# install mysql packages for python
sudo yum install mysql-devel gcc gcc-devel python-devel -y
sudo easy_install mysql-python
# Add all django required packages to a file
echo 'certifi==2018.4.16
chardet==3.0.4
diff-match-patch==20121119
django==2.1
django-admin-rangefilter==0.3.0
django-auditlog==0.4.4
django-crum==0.7.2
django-environ==0.4.4
django-filter==2.1.0
django-import-export==1.0.0
django-jsonfield==1.0.1
django-simple-history==2.7.0
django-smtp-ssl==1.0
djangorestframework==3.7.3
drfapikey==0.0.3
et-xmlfile==1.0.1
get==1.0.2
idna==2.6
jdcal==1.4
Markdown==2.6.10
mysqlclient==1.3.12
odfpy==1.3.6
openpyxl==2.5.3
post==1.0.1
public==1.0.2
PyMySQL==0.8.0
python-dateutil==2.6.0
pytz==2017.3
PyYAML==3.12
query-string==1.0.1
requests==2.18.4
six==1.11.0
urllib3==1.22
request==1.0.1
schedule==0.5.0
tablib==0.12.1
unicodecsv==0.14.1
xlrd==1.1.0
xlwt==1.3.0
django-admin-lightweight-date-hierarchy==0.3.0
gunicorn==19.9.0
whitenoise==4.0' >/home/djangouser/requirments.txt
# install all packages from the file
python36 -m pip  install --upgrade -r /home/djangouser/requirments.txt
mkdir /home/djangouser/startups/
# create gunicorn script file
echo '#!/bin/bash
DATETIME=$(date +%Y%m%d%H%M%S)
cd /home/djangouser/
if [ -f "gunicorn_access.log" ]
then
    mv "gunicorn_access.log" "gunicorn_access_${DATETIME}.log"
fi
if [ -f "gunicorn_error.log" ]
then
    mv "gunicorn_error.log" "gunicorn_error_${DATETIME}.log"
fi
cd /home/djangouser/
. venvs/django-py36/bin/activate
cd /home/djangouser/djangoadmin/djangodemo/
gunicorn --log-level info --access-logfile ~/gunicorn_access.log --error-logfile ~/gunicorn_error.log --capture-output --enable-stdio-inheritance -p ~/gunicorn.pid -w 3 -b 0.0.0.0:80 -D djangodemo.wsgi:application' >/home/djangouser/startups/django_run.sh
# active virtualenv again
source /home/djangouser/venvs/django-py36/bin/activate
cd /home/djangouser/djangoadmin/djangodemo
# change DB host to localhost
#sed -ie 's/3.16.161.235/localhost/g' /home/djangouser/djangoadmin/djangodemo/djangodemo/.env
# migrate all tables
python36 manage.py migrate
python36 manage.py makemigrations
# create django admin superuser
python36 manage.py createsuperuser
# run gunicorn script as djangouser
chmod +x /home/djangouser/startups/django_run.sh
chown djangouser:djangouser /home/djangouser/startups/django_run.sh
sudo /home/djangouser/startups/django_run.sh