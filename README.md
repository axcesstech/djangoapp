# djangoapp
This was built and tested on amazon RHEL 7.6 linux instance. Please do revert if you find issues in a non aws RHEL while installing.

This creates a user called "djangouser" and the application resides in this directory which is "/home/djangouser". 

You can set a password for this user with cmd "passwd djangouser" from root user if required.

This repo consist of following file/folders:

1. django-install.sh // this will install the django application and create couple of supporting files in the django folder 

2. djangoadmin // this consist of all the files/codes which the application requires


##How to install ?
##Install git if you don't have already
sudo yum install git
##Clone the repo to your local server on /tmp directory
cd /tmp

git clone https://shreevishnu@bitbucket.org/axcesstech/djangoapp.git
##Change the file permission for Install_django.sh file to execute the file
cd djangoapp/

sudo chmod 770 Install_django.sh

##run the Install_django.sh script as sudo or from root user
sudo ./Install_django.sh
##Once the script is completed the installation it will ask to enter the username,emailid and password for djangoadmin.
Username (leave blank to use 'root'): desired user name 

Email address: not required can be blank

Password: desired password

Password (again): reenter the desired password


##How to access the applciation
http://serverip/admin/

Ex: http://13.59.121.137/admin/ 

Enter the username and password which you entered while setting up the applcaition. 

This application is currently running on port 80
